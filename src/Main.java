import javax.swing.*;
import java.awt.*;
import java.io.File;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Muj nazev okna");

        DrawingPanel dp = new DrawingPanel();
        dp.setPreferredSize(new Dimension(906,851));
        frame.add(dp);
        frame.pack();

        frame.setResizable(false);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }
}
