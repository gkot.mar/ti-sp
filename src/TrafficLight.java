import java.awt.*;
import java.awt.image.*;
import java.util.*;

class TrafficLight{
	private BufferedImage currentState, lightBack;
	
	private BufferedImage[] lights;

	private TLStateChanger stateChanger;
	
	private int width,height;

	private boolean[] currentStatesArray;

	public TrafficLight(int width, int height, BufferedImage baseImage, BufferedImage[] lightTypesImages, int[] currentTypes, TLStateChanger stateChanger, Color[] lightsColors){

		if(!validateParams(width, height, currentTypes, lightsColors, baseImage, lightTypesImages)){
			throw new IllegalArgumentException();
		}

		currentState = new BufferedImage(width * baseImage.getWidth(), height * baseImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
		this.width = width;
		this.height = height;

		createLightBack(baseImage, currentTypes);

		generateLights(lightTypesImages, lightsColors, currentTypes);

		if (!stateChanger.changeTLState(this))
			throw new IllegalArgumentException();

		this.stateChanger = stateChanger;
	}
	
	public TrafficLight(int width, int height, BufferedImage baseImage, BufferedImage[] lightTypesImages, int[] currentTypes, boolean[] initState, Color[] lightsColors){
		this(width, height,baseImage,lightTypesImages, currentTypes, new TLStateChanger(new ArrayList<>(){ {add(initState);} }),lightsColors);
	}

	public TrafficLight(int width, int height, BufferedImage baseImage, BufferedImage[] lightTypesImages, int[] currentTypes, Color[] lightsColors){
		this(width, height,baseImage,lightTypesImages, currentTypes, new TLStateChanger(new ArrayList<>(){ {add(new boolean[width*height]);} }),lightsColors);
	}

	private void createLightBack(BufferedImage baseImage, int[] lightTypes)
	{
		lightBack = new BufferedImage(baseImage.getWidth() * width, baseImage.getHeight() * height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = lightBack.createGraphics();

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				if (lightTypes[x*height + y] >= 0)
				{
					g.drawImage(baseImage,null, x * baseImage.getWidth(), y * baseImage.getHeight());
				}
			}
		}
	}
	
	private void generateLights(BufferedImage[] lightsTypes, Color[] colors, int[] currentTypes){
		lights = new BufferedImage[colors.length];
		
		for(int i = 0; i < colors.length; i++){
			int type = currentTypes[i];
			if (type == -1)
				continue;
			lights[i] = recolor(lightsTypes[0], colors[i]);

			if (type > 0)
				lights[i].createGraphics().drawImage(lightsTypes[type], null, 0,0);
		}
	}

	private static BufferedImage recolor(BufferedImage src, Color sc)
	{
		float[][] colorMatrix =
			{
				{ ((float)sc.getRed()) / 255f, 0, 0, 0 },
				{ ((float)sc.getGreen()) / 255f, 0, 0, 0 },
				{ ((float)sc.getBlue()) / 255f, 0, 0, 0 },
				{ 0f, 0f, 0f, 1f }
			};
		BandCombineOp changeColors = new BandCombineOp(colorMatrix, null);
		Raster sourceRaster = src.getRaster();
		WritableRaster displayRaster = sourceRaster.createCompatibleWritableRaster();
		changeColors.filter(sourceRaster, displayRaster);
		return new BufferedImage(src.getColorModel(), displayRaster, true, null);
	}
	
	private boolean validateParams(int width, int height, int[] currentTypes, Color[] lightColors, BufferedImage baseImage, BufferedImage[] lightTypesImages){
		if (currentTypes == null || lightColors == null || baseImage == null || lightTypesImages == null)
			return false;
		
		int lightSize = width*height;
		
		if(currentTypes.length != lightSize || lightColors.length != lightSize)
			return false;
			
		for(int i = 0 ; i < lightSize ; i++){
			if(currentTypes[i] < -1 || currentTypes[i] >= lightTypesImages.length)
				return false;
		}
		
		for(BufferedImage type : lightTypesImages){
			if(baseImage.getWidth() != type.getWidth() || baseImage.getHeight() != type.getHeight())
				return false;
		}
		
		return true;
	}
	
	public BufferedImage getCurrentState(){
		return currentState;
	}

	public void nextState(){
		stateChanger.nextTLState(this);
	}
	
	public boolean updateCurrentState(boolean[] lightsStates){
		if (lightsStates == null || lightsStates.length != width*height)
			return false;
		
		Graphics2D g = currentState.createGraphics();

		g.drawImage(lightBack, null, 0, 0);
			
		for(int x = 0 ; x < width; x++){
			for(int y = 0 ; y < height; y++){
				if(lights[x*height + y] == null)
					continue;
					
				int destX = x * lights[0].getWidth();
				int destY = y * lights[0].getHeight();
				if(lightsStates[x*height + y])
					g.drawImage(lights[x*height + y], null, destX, destY);
			}
		}

		currentStatesArray = lightsStates;
		return true;
	}

	public boolean[] getCurrentStatesArray(){
		return currentStatesArray;
	}
}