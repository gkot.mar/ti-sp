import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Timer;
import java.util.function.Supplier;

public class DrawingPanel extends JPanel {
    private final int FULL_DELAY = 2000;
    private final int SHORT_DELAY = 500;

    private final Color CLICKED_COLOR = Color.RED;
    private final Color NORMAL_COLOR = new Color(238,238,238);

    private Queue<Supplier<Integer>> states;

    private BufferedImage cBack;
    private BufferedImage lBack;
    private BufferedImage lBase;
    private BufferedImage lDirArrow;
    private BufferedImage lWalkStop;
    private BufferedImage lWalkWalk;

    private TrafficLight tLight1;
    private TrafficLight tLight2;
    private TrafficLight tLight3;
    private TrafficLight tLight4;
    private TrafficLight tLight5;
    private TrafficLight tLight6;
    private TrafficLight tLight7;

    private TrafficLight wLight1;
    private TrafficLight wLight2;
    private TrafficLight wLight3;
    private TrafficLight wLight4;

    private Timer t;
    private int currentDelay = 0;

    private JButton l1Button;
    private JButton l2Button;
    private JButton t1Button;
    private JButton t2Button;
    private JButton r1Button;
    private JButton r2Button;
    private JButton b1Button;
    private JButton b2Button;

    private boolean[] buttonsState;

    public DrawingPanel(){
        try {
            cBack = ImageIO.read(new File("./Img/crossBack.png"));
            lBack = ImageIO.read(new File("./Img/base.png"));
            lBase = ImageIO.read(new File("./Img/map.png"));
            lDirArrow = ImageIO.read(new File("./Img/dirArrow.png"));
            lWalkStop = ImageIO.read(new File("./Img/walkStop.png"));
            lWalkWalk = ImageIO.read(new File("./Img/walkWalk.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        buttonsState = new boolean[] {false, false, false, false};

        setupTL();
        setupStates();
        setupLayout();

        t = new Timer();
        tick();
    }

    private void setupLayout() {
        this.setLayout(null);

        //Left buttons
        l1Button = new JButton("");
        l2Button = new JButton("");
        l1Button.setBackground(NORMAL_COLOR);
        l2Button.setBackground(NORMAL_COLOR);
        this.add(l1Button);
        this.add(l2Button);
        l1Button.setBounds(89,184,25,25);
        l2Button.setBounds(89,378,25,25);
        l1Button.addActionListener(actionEvent -> leftButtonClick());
        l2Button.addActionListener(actionEvent -> leftButtonClick());

        //Top buttons
        t1Button = new JButton("");
        t2Button = new JButton("");
        t1Button.setBackground(NORMAL_COLOR);
        t2Button.setBackground(NORMAL_COLOR);
        this.add(t1Button);
        this.add(t2Button);
        t1Button.setBounds(170,90,25,25);
        t2Button.setBounds(593,90,25,25);
        t1Button.addActionListener(actionEvent -> topButtonClick());
        t2Button.addActionListener(actionEvent -> topButtonClick());

        //Right buttons
        r1Button = new JButton("");
        r2Button = new JButton("");
        r1Button.setBackground(NORMAL_COLOR);
        r2Button.setBackground(NORMAL_COLOR);
        this.add(r1Button);
        this.add(r2Button);
        r1Button.setBounds(776,301,25,25);
        r2Button.setBounds(776,589,25,25);
        r1Button.addActionListener(actionEvent -> rightButtonClick());
        r2Button.addActionListener(actionEvent -> rightButtonClick());

        //Bottom buttons
        b1Button = new JButton("");
        b2Button = new JButton("");
        b1Button.setBackground(NORMAL_COLOR);
        b2Button.setBackground(NORMAL_COLOR);
        this.add(b1Button);
        this.add(b2Button);
        b1Button.setBounds(603,735,25,25);
        b2Button.setBounds(170,735,25,25);
        b1Button.addActionListener(actionEvent -> bottomButtonClick());
        b2Button.addActionListener(actionEvent -> bottomButtonClick());
    }

    private void leftButtonClick(){
        if(wLight1.getCurrentStatesArray()[1])
            return;
        l1Button.setBackground(CLICKED_COLOR);
        l2Button.setBackground(CLICKED_COLOR);

        buttonsState[0] = true;
    }

    private void rightButtonClick(){
        if(wLight3.getCurrentStatesArray()[1])
            return;
        r1Button.setBackground(CLICKED_COLOR);
        r2Button.setBackground(CLICKED_COLOR);

        buttonsState[2] = true;
    }

    private void topButtonClick(){
        if(wLight2.getCurrentStatesArray()[1])
            return;
        t1Button.setBackground(CLICKED_COLOR);
        t2Button.setBackground(CLICKED_COLOR);

        buttonsState[1] = true;
    }

    private void bottomButtonClick(){
        if(wLight4.getCurrentStatesArray()[1])
            return;
        b1Button.setBackground(CLICKED_COLOR);
        b2Button.setBackground(CLICKED_COLOR);

        buttonsState[3] = true;
    }

    private void setupStates(){
        states = new LinkedList<>();
        states.add(this::stateA);
        states.add(this::stateAtoB);
        states.add(this::stateB);
        states.add(this::stateBtoC);
        states.add(this::stateC);
        states.add(this::stateCtoD);
        states.add(this::stateD);
        states.add(this::stateDtoE);
        states.add(this::stateE);
        states.add(this::stateEtoF);
        states.add(this::stateF);
        states.add(this::stateFtoA);
    }

    private void setupTL() {
        BufferedImage[] imgSet = new BufferedImage[] { lBase, lDirArrow, lWalkStop, lWalkWalk };

        tLight1 = new TrafficLight(2,3, lBack, imgSet, new int[] {0,0,0,1,-1,-1}, new Color[]{Color.RED, Color.YELLOW, Color.GREEN, Color.GREEN, null, null});
        tLight2 = new TrafficLight(1,3, lBack, imgSet, new int[] {0,0,0}, new Color[]{Color.RED, Color.YELLOW, Color.GREEN});
        tLight3 = new TrafficLight(1,3, lBack, imgSet, new int[] {0,0,0}, new Color[]{Color.RED, Color.YELLOW, Color.GREEN});
        tLight4 = new TrafficLight(1,3, lBack, imgSet, new int[] {0,0,0}, new Color[]{Color.RED, Color.YELLOW, Color.GREEN});
        tLight5 = new TrafficLight(1,3, lBack, imgSet, new int[] {0,0,0}, new Color[]{Color.RED, Color.YELLOW, Color.GREEN});
        tLight6 = new TrafficLight(2,3, lBack, imgSet, new int[] {0,0,0,1,-1,-1}, new Color[]{Color.RED, Color.YELLOW, Color.GREEN, Color.GREEN, null, null});
        tLight7 = new TrafficLight(1,3, lBack, imgSet, new int[] {0,0,0}, new Color[]{Color.RED, Color.YELLOW, Color.GREEN});

        //walk
        wLight1 = new TrafficLight(1,2, lBack, imgSet, new int[] {2, 3}, new Color[] {Color.RED, Color.GREEN});
        wLight2 = new TrafficLight(1,2, lBack, imgSet, new int[] {2, 3}, new Color[] {Color.RED, Color.GREEN});
        wLight3 = new TrafficLight(1,2, lBack, imgSet, new int[] {2, 3}, new Color[] {Color.RED, Color.GREEN});
        wLight4 = new TrafficLight(1,2, lBack, imgSet, new int[] {2, 3}, new Color[] {Color.RED, Color.GREEN});
    }

    private boolean[] createStateArray(int stateSize, int[] enabled){
        boolean[] state = new boolean[stateSize];

        for (int i : enabled){
            if (i < stateSize){
                state[i] = true;
            }
        }

        return state;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(cBack, 0, 0, null);

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        drawTransformed(g2, tLight1.getCurrentState(), 521, 755, 0, 0.3f, 0.3f);
        drawTransformed(g2, tLight2.getCurrentState(), 451, 755, 0, 0.3f, 0.3f);
        drawTransformed(g2, tLight3.getCurrentState(), 95, 320, Math.PI/2, 0.3f, 0.3f);
        drawTransformed(g2, tLight4.getCurrentState(), 256, 95, Math.PI, 0.3f, 0.3f);
        drawTransformed(g2, tLight5.getCurrentState(), 336, 95, Math.PI, 0.3f, 0.3f);
        drawTransformed(g2, tLight6.getCurrentState(), 800, 427, Math.PI/180*-66.4f, 0.3f, 0.3f);
        drawTransformed(g2, tLight7.getCurrentState(), 800, 485, Math.PI/180*-66.4f, 0.3f, 0.3f);
        //walk 1
        drawTransformed(g2, wLight1.getCurrentState(), 114, 152, 0, 0.3f, 0.3f);
        drawTransformed(g2, wLight1.getCurrentState(), 144, 435, Math.PI, 0.3f, 0.3f);
        //walk 2
        drawTransformed(g2, wLight2.getCurrentState(), 139, 145, -Math.PI/2, 0.3f, 0.3f);
        drawTransformed(g2, wLight2.getCurrentState(), 650, 115, Math.PI/2, 0.3f, 0.3f);
        //walk 3
        drawTransformed(g2, wLight3.getCurrentState(), 746, 272, 0, 0.3f, 0.3f);
        drawTransformed(g2, wLight3.getCurrentState(), 776, 637, Math.PI, 0.3f, 0.3f);
        //walk 4
        drawTransformed(g2, wLight4.getCurrentState(), 139, 735, -Math.PI/2, 0.3f, 0.3f);
        drawTransformed(g2, wLight4.getCurrentState(), 656, 705, Math.PI/2, 0.3f, 0.3f);


        g2.setColor(Color.RED);
        Font font = new Font("Serif", Font.BOLD, 20);
        g2.setFont(font);

        g2.drawString("Timer: " + currentDelay, 10,30);
    }

    private void drawTransformed(Graphics2D g, BufferedImage image, int x, int y, double angle, float scaleX, float scaleY) {
        AffineTransform defTrans = g.getTransform();

        g.translate(x, y);
        g.rotate(angle);
        g.scale(scaleX, scaleY);

        g.drawImage(image, 0,0,null);
        g.setTransform(defTrans);
    }

    private void tick(){
        Supplier<Integer> current = states.remove();
        Integer nextWait = current.get();
        states.add(current);

        boolean result = false;
        for (boolean temp: buttonsState) {
            result |= temp;
        }
        currentDelay = result ? nextWait / 2 : nextWait;

        t.schedule(new TimerTask() {
            @Override
            public void run() {
                tick();
            }
        }, currentDelay);

        this.repaint();
    }

    // State transition methods

    private int stateA(){
        tLight1.updateCurrentState(createStateArray(6, new int[] { 2, 3 }));
        tLight2.updateCurrentState(createStateArray(3, new int[] { 2 }));

        tLight3.updateCurrentState(createStateArray(3, new int[] { 0 }));

        tLight4.updateCurrentState(createStateArray(3, new int[] { 2 }));
        tLight5.updateCurrentState(createStateArray(3, new int[] { 0 }));

        tLight6.updateCurrentState(createStateArray(6, new int[] { 0 }));
        tLight7.updateCurrentState(createStateArray(3, new int[] { 0 }));

        //walk
        wLight1.updateCurrentState(createStateArray(2, new int[] { 1 }));
        wLight2.updateCurrentState(createStateArray(2, new int[] { 0 }));
        wLight3.updateCurrentState(createStateArray(2, new int[] { 0 }));
        wLight4.updateCurrentState(createStateArray(2, new int[] { 0 }));

        return FULL_DELAY;
    }

    private int stateAtoB(){
        tLight1.updateCurrentState(createStateArray(6, new int[] { 1 }));
        tLight2.updateCurrentState(createStateArray(3, new int[] { 1 }));
        tLight5.updateCurrentState(createStateArray(3, new int[] { 1 }));

        return SHORT_DELAY;
    }

    private int stateB(){
        tLight1.updateCurrentState(createStateArray(6, new int[] { 0 }));
        tLight2.updateCurrentState(createStateArray(3, new int[] { 0 }));
        tLight5.updateCurrentState(createStateArray(3, new int[] { 2 }));
        tLight6.updateCurrentState(createStateArray(6, new int[] { 0, 3 }));

        return FULL_DELAY;
    }

    private int stateBtoC(){
        tLight4.updateCurrentState(createStateArray(3, new int[] { 1 }));
        tLight5.updateCurrentState(createStateArray(3, new int[] { 1 }));
        tLight6.updateCurrentState(createStateArray(6, new int[] { 1, 3 }));
        tLight7.updateCurrentState(createStateArray(3, new int[] { 1 }));
        // Walk
        wLight1.updateCurrentState(createStateArray(2, new int[] { 0 }));

        return SHORT_DELAY;
    }

    private int stateC(){
        tLight4.updateCurrentState(createStateArray(3, new int[] { 0 }));
        tLight5.updateCurrentState(createStateArray(3, new int[] { 0 }));
        tLight6.updateCurrentState(createStateArray(6, new int[] { 2, 3 }));
        tLight7.updateCurrentState(createStateArray(3, new int[] { 2 }));

        return FULL_DELAY;
    }

    private int stateCtoD(){
        tLight3.updateCurrentState(createStateArray(3, new int[] { 1 }));
        tLight7.updateCurrentState(createStateArray(3, new int[] { 1 }));

        return SHORT_DELAY;
    }

    private int stateD(){
        tLight3.updateCurrentState(createStateArray(3, new int[] { 2 }));
        tLight7.updateCurrentState(createStateArray(3, new int[] { 0 }));
        // Walk
        wLight4.updateCurrentState(createStateArray(2, new int[] { 1 }));

        buttonsState[3] = false;
        b1Button.setBackground(NORMAL_COLOR);
        b2Button.setBackground(NORMAL_COLOR);

        return FULL_DELAY;
    }

    private int stateDtoE(){
        tLight6.updateCurrentState(createStateArray(6, new int[] { 2 }));

        return SHORT_DELAY;
    }

    private int stateE(){
        wLight2.updateCurrentState(createStateArray(2, new int[] { 1 }));

        buttonsState[1] = false;
        t1Button.setBackground(NORMAL_COLOR);
        t2Button.setBackground(NORMAL_COLOR);

        return FULL_DELAY;
    }

    private int stateEtoF(){
        tLight1.updateCurrentState(createStateArray(6, new int[] { 1 }));
        tLight2.updateCurrentState(createStateArray(3, new int[] { 1 }));
        tLight3.updateCurrentState(createStateArray(3, new int[] { 1 }));
        tLight4.updateCurrentState(createStateArray(3, new int[] { 1 }));
        tLight6.updateCurrentState(createStateArray(6, new int[] { 1 }));
        // Walk
        wLight2.updateCurrentState(createStateArray(2, new int[] { 0 }));
        wLight4.updateCurrentState(createStateArray(2, new int[] { 0 }));

        return SHORT_DELAY;
    }

    private int stateF(){
        tLight1.updateCurrentState(createStateArray(6, new int[] { 2 }));
        tLight2.updateCurrentState(createStateArray(3, new int[] { 2 }));
        tLight3.updateCurrentState(createStateArray(3, new int[] { 0 }));
        tLight4.updateCurrentState(createStateArray(3, new int[] { 2 }));
        tLight6.updateCurrentState(createStateArray(6, new int[] { 0 }));
        // Walk
        wLight1.updateCurrentState(createStateArray(2, new int[] { 1 }));
        wLight3.updateCurrentState(createStateArray(2, new int[] { 1 }));

        buttonsState[0] = false;
        l1Button.setBackground(NORMAL_COLOR);
        l2Button.setBackground(NORMAL_COLOR);

        buttonsState[2] = false;
        r1Button.setBackground(NORMAL_COLOR);
        r2Button.setBackground(NORMAL_COLOR);

        return FULL_DELAY;
    }

    private int stateFtoA(){
        wLight3.updateCurrentState(createStateArray(2, new int[] { 0 }));

        return SHORT_DELAY;
    }
}
