import java.util.List;

public class TLStateChanger {

    private List<boolean[]> lightStateSets;

    private int currentState;

    public TLStateChanger(List<boolean[]> lightStateSets){
        this.lightStateSets = lightStateSets;
        currentState = 0;
    }

    public boolean changeTLState(TrafficLight tl){
        return tl.updateCurrentState(lightStateSets.get(currentState));
    }

    public boolean nextTLState(TrafficLight tl){
        currentState = (currentState + 1) % lightStateSets.size();

        return changeTLState(tl);
    }
}
